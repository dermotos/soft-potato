//
//  SPMovie.h
//  Soft Potato
//
//  Created by Maxime Cattet on 21/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBJson.h"
#import "SPFunctions.h"
#import "SPServersManager.h"

@interface SPMovie : NSObject

@property (assign, nonatomic) NSInteger libraryID;
@property (strong, nonatomic) NSString *imdb;
@property (strong, nonatomic) NSString *title;
@property (assign, nonatomic) NSInteger year;
@property (strong, nonatomic) NSURL *posterURL;
@property (strong, nonatomic) UIImage *poster;
@property (strong, nonatomic) NSURL *backdropURL;
@property (strong, nonatomic) UIImage *backdrop;
@property (strong, nonatomic) NSString *rating;
@property (strong, nonatomic) NSString *plot;
@property (strong, nonatomic) NSMutableArray *genres;
@property (strong, nonatomic) NSString *tagline;
@property (strong, nonatomic) NSMutableArray *directors;
@property (strong, nonatomic) NSMutableDictionary *actors;
@property (strong, nonatomic) NSString *quality;

- (id)initWithNSDictionnary:(NSDictionary*)movie;
- (id)initFromQueryWithNSDictionnary:(NSDictionary *)movie;
- (void)downloadMovieData;

@end
