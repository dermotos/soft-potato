//
//  SPFunctions.m
//  Soft Potato
//
//  Created by Maxime Cattet on 22/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import "SPFunctions.h"

@implementation SPFunctions

+ (id)getJsonFromURL:(NSURL *)url{
    NSData *data = [NSData dataWithContentsOfURL:url];
    SBJsonParser *jsonParse = [[SBJsonParser alloc] init];
    return [jsonParse objectWithData:data];
}

+ (BOOL)connectedToInternetWithURL:(NSURL *)url
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSHTTPURLResponse *response;
    
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
    
    return ([response statusCode] == 200) ? YES : NO;
}

@end
