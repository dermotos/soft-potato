//
//  SPFunctions.h
//  Soft Potato
//
//  Created by Maxime Cattet on 22/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBJson.h"

@interface SPFunctions : NSObject

+ (id)getJsonFromURL:(NSURL*)url;
+ (BOOL)connectedToInternetWithURL:(NSURL *)url;

@end
