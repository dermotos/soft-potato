//
//  SPMovie.m
//  Soft Potato
//
//  Created by Maxime Cattet on 21/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import "SPMovie.h"

@implementation SPMovie

- (id)initWithNSDictionnary:(NSDictionary *)movie{
    if (self = [super init]){
        _genres = [[NSMutableArray alloc] init];
        _actors = [[NSMutableDictionary alloc] init];
        _directors = [[NSMutableArray alloc] init];
        
        _libraryID = [movie[@"library_id"] integerValue];
        
        NSDictionary *library = movie[@"library"];
        
        if (library == nil){
            library = movie;
        }
        
        _plot = library[@"plot"];
        _tagline = library[@"tagline"];
        
        NSDictionary *info = library[@"info"];
        
        if (info == nil){
            info = movie;
        }
        
        _title = info[@"original_title"];
        _rating = [NSString stringWithFormat:@"%@", info[@"rating"][@"imdb"][0]];
        _imdb = info[@"imdb"];
        _year = [info[@"year"] integerValue];
        
        for (NSString *director in info[@"directors"]) {
            [_directors addObject:director];
        }
        
        NSArray *actorsKeys = [info[@"actor_roles"] allKeys];
        for (NSString *key in actorsKeys) {
            [_actors setValue:key forKey:[info[@"actor_roles"] objectForKey:key]];
        }
        
        for (NSString *genre in info[@"genres"]) {
            [_genres addObject:genre];
        }
        
        NSDictionary *images = info[@"images"];
        NSArray *posters = images[@"poster"];
        if (posters.count != 0){
            _posterURL = [NSURL URLWithString:posters[0]];
        }
        
        NSArray *backdrop_original = images[@"backdrop_original"];
        if (backdrop_original.count != 0){
            _backdropURL = [NSURL URLWithString:images[@"backdrop_original"][0]];
        }
        
//        [self downloadMovieData];
    }
    return self;
}

- (id)initFromQueryWithNSDictionnary:(NSDictionary *)movie{
    if (self = [super init]){
        _genres = [[NSMutableArray alloc] init];
        _actors = [[NSMutableDictionary alloc] init];
        _directors = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)downloadMovieData{
    NSString *string = [NSString stringWithFormat:@"%@/media.get/?id=%d",[[[SPServersManager shared] currentServer] getFullURL], _libraryID];
    NSURL *moviesURL = [NSURL URLWithString:string];
    NSDictionary *jsonObjects = [SPFunctions getJsonFromURL:moviesURL];
    
    _quality = jsonObjects[@"media"][@"profile"][@"types"][0][@"quality"][@"label"];
}

@end
