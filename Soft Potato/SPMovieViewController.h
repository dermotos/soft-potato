//
//  SPMovieViewController.h
//  Soft Potato
//
//  Created by Maxime Cattet on 22/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBJson.h"
#import "SPMovie.h"

@interface SPMovieViewController : UIViewController <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *movieTitle;
@property (weak, nonatomic) IBOutlet UIImageView *poster;
@property (weak, nonatomic) IBOutlet UILabel *year;
@property (weak, nonatomic) IBOutlet UILabel *rating;
@property (weak, nonatomic) IBOutlet UILabel *tagline;
@property (weak, nonatomic) IBOutlet UILabel *quality;
@property (weak, nonatomic) IBOutlet UITextView *plot;
@property (weak, nonatomic) IBOutlet UITextView *actors;
@property (weak, nonatomic) IBOutlet UILabel *imdbLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *trashButton;

@property (strong, nonatomic) SPMovie *movie;
- (IBAction)TrashButtonTouched:(UIBarButtonItem *)sender;

@end
