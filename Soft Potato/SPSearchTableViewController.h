//
//  SPSearchTableViewController.h
//  Soft Potato
//
//  Created by Maxime Cattet on 24/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPMovie.h"
#import "SPMovieSearchTableViewCell.h"
#import "SPFunctions.h"
#import "SPServersManager.h"

@interface SPSearchTableViewController : UITableViewController <UITextFieldDelegate, UIScrollViewDelegate, UIActionSheetDelegate> {
    NSMutableArray *searchResults;
    NSMutableArray *qualities;
    SPMovie *selectedMovie;
}
@property (strong, nonatomic) IBOutlet UITextField *searchTextField;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

- (IBAction)searchTextFieldEditingChanged:(UITextField *)sender;

@end
