//
//  SPServerTableViewController.h
//  Soft Potato
//
//  Created by Maxime Cattet on 22/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPServer.h"
#import "SPServersManager.h"
#import "NSString+MD5.h"
#import "SPFunctions.h"

@interface SPServerTableViewController : UITableViewController {
    IBOutlet UITextField *name;
    IBOutlet UITextField *url;
    IBOutlet UITextField *port;
    IBOutlet UITextField *login;
    IBOutlet UITextField *password;
    IBOutlet UITextField *apikey;
    IBOutlet UIBarButtonItem *EditBarButtonItem;
    IBOutlet UIButton *getApiKeyButton;
    
    IBOutlet UIButton *DeleteButton;
    IBOutlet UITableViewCell *saveTableViewCell;
    IBOutlet UIButton *saveButton;
    IBOutlet UITableViewCell *deleteTableViewCell;
    
    BOOL edit;
    NSInteger serverID;
}

@property (strong, nonatomic) SPServer *server;

- (void)setServer:(SPServer*)server andID:(NSInteger)ID;
- (IBAction)EditButtonTouch:(id)sender;
- (IBAction)SaveButtonTouch:(id)sender;
- (IBAction)getAPIKeyTouch:(UIButton *)sender;
- (IBAction)DeleteServerTouched:(id)sender;

@end
