//
//  SPMovieSearchTableViewCell.m
//  Soft Potato
//
//  Created by Maxime Cattet on 24/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import "SPMovieSearchTableViewCell.h"

@implementation SPMovieSearchTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
