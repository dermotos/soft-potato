//
//  SPMovieSearchTableViewCell.h
//  Soft Potato
//
//  Created by Maxime Cattet on 24/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPMovieSearchTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *poster;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *rating;
@property (strong, nonatomic) IBOutlet UILabel *year;

@end
