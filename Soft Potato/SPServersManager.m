//
//  SPServersManager.m
//  Soft Potato
//
//  Created by Maxime Cattet on 22/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import "SPServersManager.h"

@implementation SPServersManager
static SPServersManager *shared_serversManager = nil;

- (id)init{
    if (self = [super init]){
        _currentServer = nil;
        _servers = [[NSMutableArray alloc] init];
    }
    return self;
}

+ (SPServersManager*)shared{
    if (shared_serversManager == nil){
        shared_serversManager = [[super allocWithZone:NULL] init];
    }
    return shared_serversManager;
}

- (void)setServers:(NSMutableArray *)servers{
    _servers = servers;
}

- (void)loadServers{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    _servers = [[NSMutableArray alloc] init];
    if ([ud objectForKey:@"servers"] != nil){
        NSArray *servers = [ud objectForKey:@"servers"];
        
        for (NSDictionary *server in servers) {
            [_servers addObject:[[SPServer alloc] initWithName:server[@"name"] URL:[NSURL URLWithString:server[@"url"]]
                                                         port:[server[@"port"] integerValue]
                                                        login:server[@"login"]
                                                     password:server[@"password"]
                                                       apiKey:server[@"apikey"]]];
        }
    }
    
    if ([ud objectForKey:@"currentServer"] != nil){
        for (SPServer *server in _servers) {
            if ([server.name isEqualToString:[ud objectForKey:@"currentServer"]]){
                _currentServer = server;
            }
        }
    }
}

- (void)saveServers{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSMutableArray *servers = [[NSMutableArray alloc] init];
    
    for (SPServer *server in _servers) {
        [servers addObject:[server toNSDictionary]];
    }
    [ud setObject:servers forKey:@"servers"];
    [ud setObject:[_currentServer name] forKey:@"currentServer"];
}

@end
