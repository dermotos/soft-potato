//
//  SPSettingsTableViewController.m
//  Soft Potato
//
//  Created by Maxime Cattet on 22/03/2014.
//  Copyright (c) 2014 Maxime Cattet. All rights reserved.
//

#import "SPSettingsTableViewController.h"

@interface SPSettingsTableViewController ()

@end

@implementation SPSettingsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


- (void)viewDidAppear:(BOOL)animated{
}

- (void)viewWillAppear:(BOOL)animated
{
    [[SPServersManager shared] loadServers];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [SPServersManager shared].servers.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section == 0)
        return @"CouchPotato Servers";
    else
        return @"";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SPServerTableViewCell *cell = (SPServerTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ServerCell" forIndexPath:indexPath];
    
    SPServer *server = [[SPServersManager shared].servers objectAtIndex:indexPath.row];
    cell.serverName.text = server.name;
    
    if ([selectedRow isEqual:indexPath] || server == [[SPServersManager shared] currentServer]){
        cell.serverSwitch.on = true;
        selectedRow = indexPath;
    }
    else{
        cell.serverSwitch.on = false;
    }
    
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowServer"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        SPServer *server = [[SPServersManager shared].servers objectAtIndex:indexPath.row];
        [[segue destinationViewController] setServer:server andID:indexPath.row];
    }
    else if ([[segue identifier] isEqualToString:@"NewServer"]){
        [[segue destinationViewController] setServer:[[SPServer alloc] init] andID:[SPServersManager shared].servers.count];
    }
}

- (IBAction)SwitchSwitched:(UISwitch*)sender {
    CGPoint center= sender.center;
    CGPoint rootViewPoint = [sender.superview convertPoint:center toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:rootViewPoint];
//    NSLog(@"%d",indexPath.row);
    
    [SPServersManager shared].currentServer = [[SPServersManager shared].servers objectAtIndex:indexPath.row];
//    NSLog(@"%@", [SPServersManager shared].currentServer.name);
    
    if (indexPath.row != selectedRow.row){
        SPServerTableViewCell *cell = (SPServerTableViewCell*)[self.tableView cellForRowAtIndexPath:selectedRow];
        cell.serverSwitch.on = false;
    }
    
    selectedRow = indexPath;
}
@end
